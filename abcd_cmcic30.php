<?php

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;


if (!defined('_PS_VERSION_')) {
    exit;
}

/*
 *
 * Module de paiement CM CIC.
 *
 * Il s'agit d'une adaptation des sources fourni par la plateforme technique de paiement du Crédit mutuel, CIC
 * http://www.cmcicpaiement.fr/fr/index.html?origine=CIC
 * par T.S
 * Mise à jour pour la plateforme monetico et prestashop 1.7
 */
class abcd_CMCIC30 extends PaymentModule
{

    private $_html = '';
    private $_postErrors = array();
    // ============================================================
    //
    // Partie I : fonctions propres à l'installation et au parametrage du module
    //
    // ============================================================

    public function __construct() {
        $this->name = 'abcd_cmcic30';
        $this->tab = 'payments_gateways';
        $this->version = '1.7';
        $this->author = 'Anonymous';
        $this->controllers = array('payment', 'validation');

		$this->bootstrap = true;
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        parent::__construct();

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => '1.7.99.99');
        $this->displayName = $this->l('Paiement CIC / Monetico');
        $this->description = $this->l('Accepte les paiements Credit Mutuel - CIC');

    }

    //
    // fonction d'installation du module
    //
    public function install(){
        if (!parent::install()
        OR !Configuration::updateValue('MYBQE_TPE', '')
        OR !Configuration::updateValue('MYBQE_VERSION', '')
        OR !Configuration::updateValue('MYBQE_SERVEUR', '')
        OR !Configuration::updateValue('MYBQE_URLPAYMENT', '')
        OR !Configuration::updateValue('MYBQE_CODESOCIETE', '')
        OR !Configuration::updateValue('MYBQE_URLOK', '')
        OR !Configuration::updateValue('MYBQE_URLKO', '')
        OR !Configuration::updateValue('MYBQE_KEYPASS', '')
        OR !Configuration::updateValue('MYBQE_TEST', '')
        OR !$this->registerHook('displayPaymentEU')
        OR !$this->registerHook('payment')
        OR !$this->registerHook('paymentOptions')
        OR !$this->registerHook('paymentReturn'))
            {
                return false;
            }
        return true;
    }

    // param aucun
    public function LitKey($dir,$tp){
        $fichier=_PS_MODULE_DIR_.$this->name."/".$dir."/".trim($tp).'.key'; 

        if (file_exists($fichier)) {
            $data_cle = explode(' ',nl2br(file_get_contents($fichier)));
            return (substr($data_cle[2], 0, -3));
        } else {
            return 'cle non definie';
        }
    }

    //
    // fonction de desinstallation du module
    //
    public function uninstall(){
        if (!parent::uninstall()
            OR !Configuration::deleteByName('MYBQE_TPE', '')
            OR !Configuration::deleteByName('MYBQE_VERSION', '')
            OR !Configuration::deleteByName('MYBQE_SERVEUR', '')
            OR !Configuration::deleteByName('MYBQE_URLPAYMENT', '')
            OR !Configuration::deleteByName('MYBQE_CODESOCIETE', '')
            OR !Configuration::deleteByName('MYBQE_URLOK', '')
            OR !Configuration::deleteByName('MYBQE_URLKO', '')
            OR !Configuration::deleteByName('MYBQE_KEYPASS', '')
            OR !Configuration::deleteByName('MYBQE_TEST', ''))
            return false;
        return true;
    }

    //
    // Fonction de verification du formulaire
    //
    public function getContent()  {
        global $currentIndex, $cookie;

        //$link = $currentIndex.'&configure='.$this->name.'&token='.Tools::getValue('token');
        $this->_html .= '<h2>Paiement Euro-Information - CIC - Crédit mutuel</h2>';

        if (Tools::isSubmit('submit'.$this->name)){
            if (empty(Tools::getValue('MYBQE_TPE')))
                $this->_postErrors[] = $this->l('Le numero de TPE est obligatoire');
            if (empty(Tools::getValue('MYBQE_VERSION')))
                $this->_postErrors[] = $this->l('La version est obligatoire (>= 3.0)');
            if (empty(Tools::getValue('MYBQE_SERVEUR')))
                $this->_postErrors[] = $this->l('Le serveur bancaire est obligatoire');
            if (empty(Tools::getValue('MYBQE_URLPAYMENT')))
                $this->_postErrors[] = $this->l('L\'url de paiememnt bancaire est obligatoire');
            if (empty(Tools::getValue('MYBQE_CODESOCIETE')))
                $this->_postErrors[] = $this->l('Le code sociéte est obligatoire');
            if (empty(Tools::getValue('MYBQE_URLOK')))
                $this->_postErrors[] = $this->l('Le champs URL OK est obligatoire');
            if (empty(Tools::getValue('MYBQE_URLKO')))
                $this->_postErrors[] = $this->l('Le champs URL KO est obligatoire');
            if (empty(Tools::getValue('MYBQE_KEYPASS')))
                $this->_postErrors[] = $this->l('Le champs repertoire de cle');

            if (!sizeof($this->_postErrors)){
                Configuration::updateValue('MYBQE_TPE', Tools::getValue('MYBQE_TPE'));
                Configuration::updateValue('MYBQE_VERSION', Tools::getValue('MYBQE_VERSION'));
                Configuration::updateValue('MYBQE_SERVEUR', Tools::getValue('MYBQE_SERVEUR'));
                Configuration::updateValue('MYBQE_URLPAYMENT', Tools::getValue('MYBQE_URLPAYMENT'));
                Configuration::updateValue('MYBQE_CODESOCIETE', Tools::getValue('MYBQE_CODESOCIETE'));
                Configuration::updateValue('MYBQE_URLOK', Tools::getValue('MYBQE_URLOK'));
                Configuration::updateValue('MYBQE_URLKO', Tools::getValue('MYBQE_URLKO'));
                Configuration::updateValue('MYBQE_KEYPASS', Tools::getValue('MYBQE_KEYPASS'));
                Configuration::updateValue('MYBQE_TEST', Tools::getValue('MYBQE_TEST'));
                $this->displayConf();
            } else {
                $this->displayErrors();
            }
        }

        $this->displayMyBqeInformation();
        $this->displayFormSettings();
        return $this->_html;
    }

    //
    // Confirme les informations saisies
    //
    public function displayConf(){
        $this->_html .= '
             <div class="conf confirm">
             <img src="../img/admin/ok.gif" alt="Confirmation des données bancaires" />
             Param&egrave;tres Sauvegard&eacute;s
             </div>';
    }

    //
    // affichage et enumeration des erreurs
    //
    public function displayErrors(){
        $nbErrors = sizeof($this->_postErrors);
        $this->_html .= '
              <div class="alert error">
                <h3>'.($nbErrors > 1 ? $this->l('La saisie comporte') : $this->l('La saisie comporte')).' '.$nbErrors.' '.($nbErrors > 1 ? $this->l('erreur(s)') : $this->l('erreur')).'</h3>
                <ul>';
        foreach ($this->_postErrors AS $error)
            $this->_html .= '<li>'.$error.'</li>';
        $this->_html .= '
                </ul>
              </div>';
    }

    //
    //
    //
    public function displayMyBqeInformation()
    {
        $this->_html .= '
               <img src="../modules/'.$this->name.'/img/admin_cmcic.png" style="float:left; margin-right:15px;" />
               <b>Ce module vous permet d\'accepter les paiements par votre banque (Crédit mutuel ou CIC )via Euro Information.</b><br /><br />
               Si le client choisis ce mode de paiement, votre compte bancaire sera automatiquement cr&eacute;dit&eacute;.<br />
               Vous devez cr&eacute;er votre compte e-commerce aupr&egrave; de votre banque via Euro Information.<br />Les donn&eacute;es inscrites par d&eacute;faut sont ici des donn&eacute;es vous permettant de faire fonctionner le module en mode g&eacute;n&eacute;rique mais en aucun cas d\'accepter de r&eacute;els paiements.
               <br /><br /><br />';
    }

    /**
     * @return array Retourne les valeur contenu dans la bdd
     */
    public function _confMYBQE()
    {
        return Configuration::getMultiple([
            'MYBQE_TPE',
            'MYBQE_VERSION',
            'MYBQE_SERVEUR',
            'MYBQE_URLPAYMENT',
            'MYBQE_CODESOCIETE',
            'MYBQE_URLOK',
            'MYBQE_URLKO',
            'MYBQE_KEYPASS',
            'MYBQE_TEST'
        ]);
    }

    //
    // Affiche la configuration
    //
    public function displayFormSettings() {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Paiement CM-CIC - via EuroInformation'),
                    'image' => '../modules/'.$this->name.'/img/logo.gif'
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'value' => 'label',
                        'label' => $this->l('Tpe'),
                        'name' => 'MYBQE_TPE',
                        'required' => true
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Version'),
                        'name' => 'MYBQE_VERSION',
                        'required' => true
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Serveur'),
                        'name' => 'MYBQE_SERVEUR',
                        'desc' => 'Avec le "/" à la fin',
                        'required' => true,
                    ],

                    [
                        'type' => 'select',
                        'label' => $this->l('Url de paiement'),
                        'name' => 'MYBQE_URLPAYMENT',
                        'required' => true,
                        'options' => [
                            'query' => [
                                [
                                    'id_MYBQE_URLPAYMENT' => 'paiement.cgi',
                                ],
                                [
                                    'id_MYBQE_URLPAYMENT' => 'capture_paiement.cgi',
                                ],
                                [
                                    'id_MYBQE_URLPAYMENT' => 'recredit_paiement.cgi',
                                ],
                                
                            ],
                            'id' => 'id_MYBQE_URLPAYMENT',
                            'name' => 'id_MYBQE_URLPAYMENT'
                            ]
                    ],

                    [
                        'type' => 'text',
                        'label' => $this->l('Code Société'),
                        'name' => 'MYBQE_CODESOCIETE',
                        'required' => true
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Url si paiement OK'),
                        'name' => 'MYBQE_URLOK',
                        'required' => true
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Url si paiement pas OK'),
                        'name' => 'MYBQE_URLKO',
                        'required' => true
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Chemin de la clé'),
                        'name' => 'MYBQE_KEYPASS',
                        'required' => true,
                    ],

                    [
                        'type' => 'radio',
                        'label' => $this->l('Mode test'),
                        'name' => 'MYBQE_TEST',
                        'required' => true,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => '/test',
                                'label' => $this->l('Activé')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => NULL,
                                'label' => $this->l('Désactivé')
                            ],
                        ]
                    ],
                ],
                'submit' => [
                    'name' => 'submit'.$this->name,
                    'title' => $this->l('Sauvegarde'),
                ]
            ],
        ];

        $helper = new HelperForm();
        // Module, Token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = $lang->id;

        // title and Toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->submit_action = 'submit'.$this->name;

        // Load current value
        $conf = $this->_confMYBQE();

        foreach ($conf as $name => $val)
            $helper->fields_value[$name] = $val;
        if (empty($helper->fields_value['MYBQE_VERSION']))
            $helper->fields_value['MYBQE_VERSION'] = '3.0';
        if (empty($helper->fields_value['MYBQE_SERVEUR']))
            $helper->fields_value['MYBQE_SERVEUR'] = 'https://p.monetico-services.com/';
        if (empty($helper->fields_value['MYBQE_URLPAYMENT']))
            $helper->fields_value['MYBQE_URLPAYMENT'] = 'paiement.cgi';

        if (empty($helper->fields_value['MYBQE_URLOK']))
            $helper->fields_value['MYBQE_URLOK'] = 'https://exemple.com/index.php?controller=order-confirmation';
        if (empty($helper->fields_value['MYBQE_URLKO']))
            $helper->fields_value['MYBQE_URLKO'] = 'https://exemple.com/index.php?controller=order';
        if (empty($helper->fields_value['MYBQE_KEYPASS']))
            $helper->fields_value['MYBQE_KEYPASS'] = 'repertoire';
        
        $this->_html .= $helper->generateForm([$fields_form]);
        return $this->_html;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        // if (!$this->checkCurrency($params['cart'])) {
        //     return;
        // }

        $payment_options = [
            $this->hookPayment($params),
        ];

        return $payment_options;
    }

    //
    // Cette fonction est utilisée lors du choix d'un paiement
    // afin d'eviter un ecran
    //  Attention dans la solution standard et vendue les données apparaissent en Hidden
    // cette proc doit donc creer un formulaire 'en live' et le soumettre au cic
    //
    public function hookPayment($params)
    {
        global $cart;

        $customer = new Customer(intval($params['cart']->id_customer));

        $confMYBQE = $this->_confMYBQE();

        //
        // Configuration librairies CIC
        //
        $wReference = $cart->id.'D'.date("dmHi");
        $wMontant = $cart->getOrderTotal();
        $wDevise  = "EUR";
        $wTexteLibre = "";
        $wLangue = "FR";
        $wEmail = $customer->email;
        //
        // on inclus les librairies du CIC
        //
        require_once('monetico/Phase1Go.php');

        $opts = [
            'version'        => $oEpt->sVersion,
            'TPE'            => $oEpt->sNumero,
            'date'           => $sDate,
            'montant'        => $sMontant . $sDevise,
            'reference'      => $sReference,
            'MAC'            => $sMAC,
            'url_retour'     => $oEpt->sUrlKO,
            'url_retour_ok'  => $oEpt->sUrlOK,
            'url_retour_err' => $oEpt->sUrlKO,
            'lgue'           => $oEpt->sLangue,
            'societe'        => $oEpt->sCodeSociete,
            'texte-libre'    => HtmlEncode($sTexteLibre),
            'mail'           => $sEmail,
        ];

        $opts_input = [];
        foreach ($opts as $k => $v) {
            $opts_input[$k] = [
                'name' =>$k,
                'type' =>'hidden',
                'value' =>$v,
            ];
        }

        $urlserveurpaiement = $confMYBQE['MYBQE_SERVEUR']
            .$confMYBQE['MYBQE_TEST']
            .'/'.$confMYBQE['MYBQE_URLPAYMENT'];

        $externalOption = new PaymentOption();
        $externalOption
            ->setCallToActionText($this->l('Payer par carte banquaire'))

            ->setAction($urlserveurpaiement)

            ->setInputs($opts_input)
            ->setAdditionalInformation($this->context->smarty->fetch('module:abcd_cmcic30/views/templates/front/payment_infos.tpl'))
            ->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/logo.gif'));
        return $externalOption;

    } // hookPayment

    // ========================================================
    // Partie II focntions propres au paiement
    // ========================================================
    // affiche l'ecran de paiement

    // lit la monnaie utilisée dans un panier
 	private function _checkCurrency($cart)
	{
		$currency_order = new Currency(intval($cart->id_currency));
		$currencies_module = $this->getCurrency();
		$currency_default = Configuration::get('PS_CURRENCY_DEFAULT');

		if (is_array($currencies_module))
			foreach ($currencies_module AS $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
	}

} // end of class
?>
