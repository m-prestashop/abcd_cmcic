{*                                                  *}
{* Template de choix de paiement et d'execution     *}
{* à destination d'Euroformation                    *}
{*                                                  *}
<p class="payment_module">
	<a href="javascript:$('#formulairebqe').submit();" title="{l s='Paiement par CB ' mod='abcd_cmcic30'}">
		<img src="{$this_path}/img/cic-paiement-demi.jpg" alt="{l s='Paiement par CB' mod='abcd_cmcic30'}" />
		{l s='Payez par CB' mod='abcd_cmcic30'}
	</a>
	<form id="formulairebqe" name="formulairebqe" action="{$serveur}" method="post">
	<input type="hidden" name="version"             id="version"        value="{$version}" />
	<input type="hidden" name="TPE"                 id="TPE"            value="{$TPE}" />
	<input type="hidden" name="date"                id="date"           value="{$date}" />
	<input type="hidden" name="montant"             id="montant"        value="{$montant}{$devise}" />
	<input type="hidden" name="reference"           id="reference"      value="{$reference}" />
	<input type="hidden" name="MAC"                 id="MAC"            value="{$MAC}" />
	<input type="hidden" name="url_retour"          id="url_retour"     value="{$url_retour}" />
	<input type="hidden" name="url_retour_ok"       id="url_retour_ok"  value="{$url_retour_ok}" />
	<input type="hidden" name="url_retour_err"      id="url_retour_err" value="{$url_retour_err}" />
	<input type="hidden" name="lgue"                id="lgue"           value="{$lgue}" />
	<input type="hidden" name="societe"             id="societe"        value="{$societe}" />
	<input type="hidden" name="texte-libre"         id="texte-libre"    value="{$textelibre}" />
	<input type="hidden" name="mail"                id="mail"           value="{$mail}" />
{*
	non utilisé
	<input type="hidden" name="nbrech"              id="nbrech"         value="<?php echo $sNbrEch;?>" />
	<input type="hidden" name="dateech1"            id="dateech1"       value="<?php echo $sDateEcheance1;?>" />
	<input type="hidden" name="montantech1"         id="montantech1"    value="<?php echo $sMontantEcheance1;?>" />
	<input type="hidden" name="dateech2"            id="dateech2"       value="<?php echo $sDateEcheance2;?>" />
	<input type="hidden" name="montantech2"         id="montantech2"    value="<?php echo $sMontantEcheance2;?>" />
	<input type="hidden" name="dateech3"            id="dateech3"       value="<?php echo $sDateEcheance3;?>" />
	<input type="hidden" name="montantech3"         id="montantech3"    value="<?php echo $sMontantEcheance3;?>" />
	<input type="hidden" name="dateech4"            id="dateech4"       value="<?php echo $sDateEcheance4;?>" />
	<input type="hidden" name="montantech4"         id="montantech4"    value="<?php echo $sMontantEcheance4;?>" />
	<!-- -->
	<input type="submit" name="bouton"              id="bouton"         value="Connexion / Connection" />
*}	
	
	</form>
</p>