abcd_cmcic Monetico v1.7
Prestashop 1.7
Origine du code : abcd_cmcic30, forum sur prestashop du module disparu, auteur Eboriac
**************************************
**************************************

Module de paiement CIC CREDIT MUTUEL
Ce module gere le paiement SIMPLE CM-CIC mais ne gere par le traitement des logs envoyés par le CMCIC (Euroinformation)) (fichier.csv) après encaissement des paiements
Les images installées sont celles fournies par le CIC , il faut juste les remplacer pour ceux qui sont chez le credit mutuel
Si vous etes au credit mutuel, je vous laisse la tache ... :)

Le paiement fractionné n'est pas géré.

Dans son fonctionnement la commande est validée dès lors que le client a validé son numero de carte
meme si ça se fait après un premier echec.
A chaque echec le client reçoit un message (envoyé par CMCIC)  (limité à 3 echecs par CM-CIC)
a la validation , il reçoit un message (ENVOYE PAR le CMCIC) et une confirmation de commande  envoyee par votre boutique prestashop.
La Commande est crée uniquement à la validation du paiement sinon la commande demeure un panier table ps_cart

NOTE ***
Je n'ai pas testé le paiement avec des devises etrangères, donc je ne peux dire s'il fonctionne dans ce cas

********

La cle doit etre deposée dans un sous repertoire de ce module
 - pour des raisons de sécurité , je vous conseille de renommer ce module (histoire que le nom ne soit pas connu à l'avance) 
 pour cela il va falloir remplacer la chaine "abcd" par une autre chaine de votre choix
 Dans notre exemple , on prendra wxyz

 1) coller le repertoire abcd_cmcic30 dans votre repertoire module (exemple wxyz_cmcic30))
 2) Renommer le repertoire abcd_cmcic30 avec  votre prefixe , par exemple wxyz_cmcic30
 3) editer les fichier abcd_cmcic30.php ,validation.php et payment-lance.tpl et remplacer toutes les occurences abcd par votre chaine (exemple wxyz))
 4) dans le repertoire , renommer de la meme maniere abcd_cmcic30.php par exemple en wxyz_cmcic30.php

 Lors de votre souscription au paiement CIC-CM l'url à fournir pour la validation est donc :
 http://www.votreboutique.com/modules/[Votre chaine]_cmcic30/validation.php

 dans notre exemple   
 http://www.votreboutique.com/modules/wxyz_cmcic30/validation.php

 5) Copiez votre fichier clé dans le repertoire du module nommé "repertoire"
 votre fichier clé doit comporter votre numero de TPE
 par exemple si votre tpe est le 12345 le fichier doit s'appeler 12345.key

 6) renommez ce repertoire  (a)
    je conseille egalement de faire un chmod 400 sur le fichier.key et 500 sur le repertoire afin d'y ajouter une protection unix
    (pour ceux qui sont sous unix)

 7) sur l'administration de prestashop installez le module
 8) Dans la configuration du module : 

 Tpe : votre code TPE
 Version : 3.0
 Serveur : l'url de test fournie par Euroformation : il s'agit de l'url sans la chaine 'Paiement.CGI
 par exemple pour le CIC en test :
 https://ssl.paiement.cic-banques.fr/test/
 code societe : votre code societe
 url de paiement si OK : http://[www.votreboutique.com]/index.php?controller=order-confirmation
 url de paiement si non OK : http://[www.votreboutique.com]/index.php?controller=order
 Ou [www.votreboutique.com] est l'url de votre boutique

 => si votre boutique est installée dans un sous repertoire, n'oubliez pas de le mentionner ici


 repertoire : le nom du repertoire ou se trouve votre cle que vous avez mis en (a)
 il s'agit juste du nom du repertoire, pas du chemin complet
 Sauvegardez votre configuration

 Il ne vous reste plus qu'à tester alors

 Si vous voulez nous aider en guise de remerciement de ce module, vous pouvez mettre un petit lien suivant :

 <a href="http://www.lesjardinsdeboriac.com">Décoration, cadeau et jardins d'intérieur</a>

 merci d'avance
 (je précise : ce n'est pas obligatoire)

 j'ai pu constater qu'en aidant une autre personne du forum  que si vous renommez tout ce qui s'appelle abcd_cmcic30 en euroinformation 
 et toute chaine de texte abcd_cmcic30 en euroinformation, alors ce module peut venir en remplacement du module euroinformation qui etait sur le forum
 pour l'ancienne version du paiement                                       
