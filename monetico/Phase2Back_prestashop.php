<?php
if ($Errorlvl1 == 'PAY-OK') {

    $id_cart = explode('D', $MoneticoPaiement_bruteVars['reference']);
    $cart = new Cart($id_cart[0]);

    $abcd_cmcic30 = Module::getInstanceByName('abcd_cmcic30');
        
    // ---------------------------------------------------------------------------
    // On valide la commande en creant l'objet 'commande' , à partir du panier, si
    // et seulement si le retour est égal à PAY-OK
    // ---------------------------------------------------------------------------
                            
    if ($cart->id_customer == 0
    || $cart->id_address_delivery == 0
    || $cart->id_address_invoice == 0
    || !$abcd_cmcic30->active) {
        $receipt = MONETICOPAIEMENT_PHASE2BACK_MACNOTOK;
    } else {

        $customer = new Customer(intval($cart->id_customer));
        if (Validate::isLoadedObject($customer)) {
                    
            $total = floatval($cart->getOrderTotal(true, Cart::BOTH));
            $id_currency = intval($cart->id_currency);

            // ne pas générer de mails car il y en a déjà un qui part
            $q = $abcd_cmcic30->validateOrder(
                intval($cart->id),
                Configuration::get('PS_OS_PAYMENT'),
                $total,
                'Credit card',

                'Code Status 3DS : '
                .$MoneticoPaiement_bruteVars['status3ds']
                .'<br>N° autorisation : '
                .$MoneticoPaiement_bruteVars['numauto']
                .'<br>N° Panier : '
                .substr($MoneticoPaiement_bruteVars['reference'],0,-8),
                  
                [],
                $id_currency,
                false,
                $customer->secure_key);
            if ($q === true) {
                $order = new Order($abcd_cmcic30->currentOrder);
                //on ne fait rien d'autre, à tester quand même
                $receipt = MONETICOPAIEMENT_PHASE2BACK_MACOK;
            } else
                $receipt = MONETICOPAIEMENT_PHASE2BACK_MACNOTOK;
                
        } else {
            $receipt = MONETICOPAIEMENT_PHASE2BACK_MACNOTOK;
        }
    }
} else {
    $receipt = MONETICOPAIEMENT_PHASE2BACK_MACNOTOK;
}
?>