<?php
/***************************************************************************************
* Warning !! MoneticoPaiement_Config contains the key, you have to protect this file with all     *   
* the mechanism available in your development environment.                             *
* You may for instance put this file in another directory and/or change its name       *
***************************************************************************************/

/* define ("MONETICOPAIEMENT_KEY", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"); */
/* define ("MONETICOPAIEMENT_EPTNUMBER", "xxxxxxx"); */
/* define ("MONETICOPAIEMENT_VERSION", "3.0"); */
/* define ("MONETICOPAIEMENT_URLSERVER", "https://p.monetico-services.com/test/"); */
/* define ("MONETICOPAIEMENT_COMPANYCODE", "masociete"); */

/* define ("MONETICOPAIEMENT_URLOK", "https://exemple.com/index.php?controller=order-confirmation"); */
/* define ("MONETICOPAIEMENT_URLKO", "https://exemple.com/index.php?controller=order"); */

/* define ("MONETICOPAIEMENT_CTLHMAC","V4.0.sha1.php--[CtlHmac%s%s]-%s"); */
/* define ("MONETICOPAIEMENT_CTLHMACSTR", "CtlHmac%s%s"); */
/* define ("MONETICOPAIEMENT_PHASE2BACK_RECEIPT","version=2\ncdr=%s"); */
/* define ("MONETICOPAIEMENT_PHASE2BACK_MACOK","0"); */
/* define ("MONETICOPAIEMENT_PHASE2BACK_MACNOTOK","1\n"); */
/* define ("MONETICOPAIEMENT_PHASE2BACK_FIELDS", "%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*"); */
/* define ("MONETICOPAIEMENT_PHASE1GO_FIELDS", "%s*%s*%s%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s"); */
/* define ("MONETICOPAIEMENT_URLPAYMENT", "paiement.cgi"); */


$abcd_cmcic30 = Module::getInstanceByName('abcd_cmcic30');
$confMYBQE = $abcd_cmcic30->_confMYBQE();

define ("MONETICOPAIEMENT_KEY",
$abcd_cmcic30->LitKey($confMYBQE['MYBQE_KEYPASS'], $confMYBQE['MYBQE_TPE']));
define ("MONETICOPAIEMENT_EPTNUMBER", $confMYBQE['MYBQE_TPE']);
define ("MONETICOPAIEMENT_VERSION", $confMYBQE['MYBQE_VERSION']);
define ("MONETICOPAIEMENT_URLSERVER", $confMYBQE['MYBQE_SERVEUR'] . $confMYBQE['MYBQE_TEST']);
define ("MONETICOPAIEMENT_COMPANYCODE", $confMYBQE['MYBQE_CODESOCIETE']);

define ("MONETICOPAIEMENT_URLOK", $confMYBQE['MYBQE_URLOK']);
define ("MONETICOPAIEMENT_URLKO", $confMYBQE['MYBQE_URLKO']);

define ("MONETICOPAIEMENT_CTLHMAC","V4.0.sha1.php--[CtlHmac%s%s]-%s");
define ("MONETICOPAIEMENT_CTLHMACSTR", "CtlHmac%s%s");
define ("MONETICOPAIEMENT_PHASE2BACK_RECEIPT","version=2\ncdr=%s");
define ("MONETICOPAIEMENT_PHASE2BACK_MACOK","0");
define ("MONETICOPAIEMENT_PHASE2BACK_MACNOTOK","1\n");
define ("MONETICOPAIEMENT_PHASE2BACK_FIELDS", "%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*");
define ("MONETICOPAIEMENT_PHASE1GO_FIELDS", "%s*%s*%s%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s*%s");
define ("MONETICOPAIEMENT_URLPAYMENT", $confMYBQE['MYBQE_URLPAYMENT']);

?>
