<?php
/*****************************************************************************
 *
 * "Open source" kit for Monetico Paiement (TM)
 *
 * File "Phase1Go.php":
 *
 * Author   : Euro-Information/e-Commerce
 * Version  : 4.0
 * Date      : 05/06/2014
 *
 * Copyright: (c) 2014 Euro-Information. All rights reserved.
 * License  : see attached document "License.txt".
 *
 *****************************************************************************/

// =============================================================================================================================================================
// SECTION INCLUDE :  Cette section ne doit pas être modifié.
// Attention !! MoneticoPaiement_Config contient la clé, vous devez protéger ce fichier avec tous les mécanismes disponibles dans votre environnement de développement.
// Vous pouvez pour l'instance mettre ce fichier dans un autre répertoire et/ou changer son nom. Dans ce cas, n'oubliez pas d'adapter le chemin d'inclusion ci-dessous.
//
// INCLUDE SECTION :  This section must not be modified
// Warning !! MoneticoPaiement_Config contains the key, you have to protect this file with all the mechanism available in your development environment.
// You may for instance put this file in another directory and/or change its name. If so, don't forget to adapt the include path below.
// =============================================================================================================================================================

require_once("MoneticoPaiement_Config.php");

// PHP implementation of RFC2104 hmac sha1 ---
require_once("MoneticoPaiement_Ept.inc.php");

// =============================================================================================================================================================
// FIN SECTION INCLUDE
//
// END INCLUDE SECTION
// =============================================================================================================================================================

// =============================================================================================================================================================
// SECTION PAIEMENT : Cette section doit être modifiée
// Ci-après, vous trouverez un exemple des valeurs requises pour effectuer un paiement en utilisant la solution Monetico Paiement.
// L'ordre des variables et le format des valeurs doivent correspondre aux spécifications techniques.
//
// PAYMENT SECTION :  This section must be modified 
// Here after, you will find an example of the values needed to demand a payment using the Monetico paiement solution.
// The order of the variables and the format of the values must follow the technical specification. 
// =============================================================================================================================================================
// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// SECTION PAIEMENT - Section générale
// 
// PAYMENT SECTION - General section
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

// Reference: unique, alphaNum (A-Z a-z 0-9), 12 characters max
$sReference = $wReference;

// Amount : format  "xxxxx.yy" (no spaces)
$sMontant = $wMontant;

// Currency : ISO 4217 compliant
$sDevise  = $wDevise;

// free texte : a bigger reference, session context for the return on the merchant website
$sTexteLibre = $wTexteLibre;

// transaction date : format d/m/y:h:m:s
$sDate = date("d/m/Y:H:i:s");

// Language of the company code
$sLangue = $wLangue;

// customer email
$sEmail = $wEmail;

$sOptions = "";

// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// SECTION PAIEMENT FRACTIONNE - Section spécifique au paiement fractionné
// 
// INSTALMENT PAYMENT SECTION - Section specific to the installment payment
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

// between 2 and 4
// entre 2 et 4
//$sNbrEch = "4";
$sNbrEch = "";

// date echeance 1 - format dd/mm/yyyy
//$sDateEcheance1 = date("d/m/Y");
$sDateEcheance1 = "";

// montant �ch�ance 1 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance1 = "0.26" . $sDevise;
$sMontantEcheance1 = "";

// date echeance 2 - format dd/mm/yyyy
$sDateEcheance2 = "";

// montant �ch�ance 2 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance2 = "0.25" . $sDevise;
$sMontantEcheance2 = "";

// date echeance 3 - format dd/mm/yyyy
$sDateEcheance3 = "";

// montant �ch�ance 3 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance3 = "0.25" . $sDevise;
$sMontantEcheance3 = "";

// date echeance 4 - format dd/mm/yyyy
$sDateEcheance4 = "";

// montant �ch�ance 4 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance4 = "0.25" . $sDevise;
$sMontantEcheance4 = "";

// =============================================================================================================================================================
// FIN SECTION PAIEMENT
//
// END PAYMENT SECTION 
// =============================================================================================================================================================

// =============================================================================================================================================================
// SECTION CODE : Cette section ne doit pas être modifiée
// 
// CODE SECTION : This section must not be modified
// =============================================================================================================================================================

$oEpt = new MoneticoPaiement_Ept($sLangue);     		
$oHmac = new MoneticoPaiement_Hmac($oEpt);      	        

// Control String for support
$CtlHmac = sprintf(
    MONETICOPAIEMENT_CTLHMAC,
    $oEpt->sVersion,
    $oEpt->sNumero,
    $oHmac->computeHmac(sprintf(MONETICOPAIEMENT_CTLHMACSTR, $oEpt->sVersion, $oEpt->sNumero)));

// Data to certify
$phase1go_fields = sprintf(
    MONETICOPAIEMENT_PHASE1GO_FIELDS,
    $oEpt->sNumero,
    $sDate,
    $sMontant,
    $sDevise,
    $sReference,
    $sTexteLibre,
    $oEpt->sVersion,
    $oEpt->sLangue,
    $oEpt->sCodeSociete, 
    $sEmail,
    $sNbrEch,
    $sDateEcheance1,
    $sMontantEcheance1,
    $sDateEcheance2,
    $sMontantEcheance2,
    $sDateEcheance3,
    $sMontantEcheance3,
    $sDateEcheance4,
    $sMontantEcheance4,
    $sOptions
);

// MAC computation
$sMAC = $oHmac->computeHmac($phase1go_fields);

// =============================================================================================================================================================
// FIN SECTION CODE
//
// END CODE SECTION 
// =============================================================================================================================================================

//require_once('Phase1Go_form_exemple.html');
?>
